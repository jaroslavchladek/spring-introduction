package cz.inventi.kpj.spring.springintroduction.configuration;

import cz.inventi.kpj.spring.springintroduction.persistence.GreetingFileRepositoryImpl;
import cz.inventi.kpj.spring.springintroduction.persistence.GreetingRepository;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
@EnableConfigurationProperties(GreetingConfigurationProperties.class)
public class GreetingConfiguration {

    @Bean(name = "greetingConstantRepositoryImpl")
    @Scope(scopeName = "prototype")
    public GreetingRepository greetingConstantRepositoryImpl() {
        return new GreetingFileRepositoryImpl();
    }

}
