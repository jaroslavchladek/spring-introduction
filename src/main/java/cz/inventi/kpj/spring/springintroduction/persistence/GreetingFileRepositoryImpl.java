package cz.inventi.kpj.spring.springintroduction.persistence;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

@Service("GreetingFileRepositoryImpl")
public class GreetingFileRepositoryImpl implements GreetingRepository {

    @Value("${spring-introduction.greeting.text}")
    private String propertiesText;

    @Override
    public String getGreetingText() {
        return propertiesText;
    }

}
