package cz.inventi.kpj.spring.springintroduction.service;

import cz.inventi.kpj.spring.springintroduction.persistence.GreetingRepository;
import cz.inventi.kpj.spring.springintroduction.service.GreetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class GreetingServiceImpl implements GreetingService {

    @Autowired
    @Qualifier("GreetingConstantRepositoryImpl")
    private GreetingRepository repositoryConstant;

    @Autowired
    @Qualifier("GreetingFileRepositoryImpl")
    private GreetingRepository repositoryFile;

    @Override
    public String getFileText() {
        return repositoryFile.getGreetingText();
    }

    @Override
    public String getConstantText() {
        return repositoryConstant.getGreetingText();
    }

}
